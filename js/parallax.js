function init() {
	main = new Main();
}

function Main() {
	//console.log('init() successfully called.');
	var width = document.getElementById('game-canvas').width;
	var height = document.getElementById('game-canvas').height;

	this.stage = new PIXI.Container();
	this.renderer = PIXI.autoDetectRenderer(width, height,
		{view:document.getElementById("game-canvas")});

	//this.scroller = new Scroller(this.stage);
	//requestAnimationFrame(this.update.bind(this));
	this.loadSpriteSheet();
}

Main.SCROLL_SPEED = 2;

Main.prototype.update = function() {
	this.scroller.moveViewportXBy(Main.SCROLL_SPEED);
	this.renderer.render(this.stage);
	requestAnimationFrame(this.update.bind(this));
};

Main.prototype.loadSpriteSheet = function() {
	var assetsToLoad = ['resources/wall.json', "resources/bg-mid.png",
		"resources/bg-far.png"];
	PIXI.loader.add(assetsToLoad)
		.load(this.spriteSheetLoaded.bind(this));
	//loader.onComplete = this.spriteSheetLoaded.bind(this);
	//loader.load();


};

Main.prototype.spriteSheetLoaded = function() {
	this.scroller = new Scroller(this.stage);
	requestAnimationFrame(this.update.bind(this));

	this.pool = new WallSpritesPool();
	this.wallSlices = [];
};

Main.prototype.generateTestWallSpan = function () {
	var lookupTable = [
		this.pool.borrowFrontEdge,
		this.pool.borrowWindow,
		this.pool.borrowDecoration,
		this.pool.borrowStep,
		this.pool.borrowWindow,
		this.pool.borrowBackEdge
	];

	var yPos = [
		128,
		128,
		128,
		192,
		192,
		192,
	];

	for (var i = 0; i < lookupTable.length; i++)
	{
		var func = lookupTable[i];

		var sprite = func.call(this.pool);
		sprite.position.x = 32 + (i * 64);
		sprite.position.y = yPos[i];

		this.wallSlices.push(sprite);

		this.stage.addChild(sprite);
	}
};

Main.prototype.clearTestWallSpan = function () {
	var lookupTable = [
		this.pool.returnFrontEdge,
		this.pool.returnWindow,
		this.pool.returnDecoration,
		this.pool.returnStep,
		this.pool.returnWindow,
		this.pool.returnBackEdge
	];

	for (var i = 0; i < lookupTable.length; i++)
	{
		var func = lookupTable[i];
		var sprite = this.wallSlices[i];

		this.stage.removeChild(sprite);
		func.call(this.pool, sprite);
	}

	this.wallSlices = [];
};

Main.prototype.borrowWallSprites = function (num) {
	for (var i = 0; i < num; i++)
	{
		var sprite;
		if (i % 2 === 0) {
			sprite = this.pool.borrowWindow();
		} else {
			sprite = this.pool.borrowDecoration();
		}
		sprite.position.x = -32 + (i * 64);
		sprite.position.y = 128;

		this.wallSlices.push(sprite);

		this.stage.addChild(sprite);
	}
};

Main.prototype.returnWallSprites = function () {
	for (var i = 0; i < this.wallSlices.length; i++)
	{
		var sprite =  this.wallSlices[i];
		this.stage.removeChild(sprite);

		if (i % 2 === 0) {
			this.pool.returnWindow(sprite);
		} else {
			this.pool.returnDecoration(sprite);
		}
	}

	this.wallSlices = [];
};

function Far() {
	var texture = PIXI.Texture.fromImage('resources/bg-far.png');
	PIXI.extras.TilingSprite.call(this, texture, 512, 256);
	this.position.x = 0;
	this.position.y = 0;
	this.tilePosition.x = 0;
	this.tilePosition.y = 0;

	this.viewportX = 0;
}

Far.constructor = Far;
Far.prototype = Object.create(PIXI.extras.TilingSprite.prototype);

Far.DELTA_X = 0.128;

Far.prototype.setViewportX = function(newViewportX) {
	var distanceTravelled = newViewportX - this.viewportX;
	this.viewportX = newViewportX;
	this.tilePosition.x -= (distanceTravelled * Far.DELTA_X);

};


function Mid() {
	var texture = PIXI.Texture.fromImage('resources/bg-mid.png');
	PIXI.extras.TilingSprite.call(this, texture, 512, 256);
	this.position.x = 0;
	this.position.y = 128;
	this.tilePosition.x = 0;
	this.tilePosition.y = 0;

	this.viewportX = 0;
}

Mid.constructor = Mid;
Mid.prototype = Object.create(PIXI.extras.TilingSprite.prototype);

Mid.DELTA_X = 0.64;

Mid.prototype.setViewportX = function(newViewportX) {
	var distanceTravelled = newViewportX - this.viewportX;
	this.viewportX = newViewportX;
	this.tilePosition.x -= (distanceTravelled * Mid.DELTA_X);
};
