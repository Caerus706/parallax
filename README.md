# Parallax Tutorial #

### Parallax Scroller Tutorial###

* This is a parallax scroller tutorial from pixijs.com resources.
* However, it is being done and automatically updated to pixijs v3
* Currently starting part 4 of tutorial

## Changes ##

Pixi.js has changed stage to Container giving a generic universal name and source for organization.
Texture loaders and TilingTextures have been changed as well (slightly)
